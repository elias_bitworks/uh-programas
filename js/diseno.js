var playerInfoList = new Array();

$(function(){
	
	//Border en columnas 
	//Quita la clase en la ultima columna de cada fila
	setBorderClass(".titulares",".item","r-border");
	$(window).resize(function(){
		setBorderClass(".titulares",".item","r-border");
	});
	
	
	//chacar si soporta SVG, de lo contrario sustituye 
	//URL de las imagenes por PNG
	if (!Modernizr.svgasimg) {
		changeSVGExtension();
	}
	
	
	//Animar label de formulario
	$(".animate-label input, .animate-label textarea").on("focusin focusout change keyup",function(e){
		var elem = $(this);
		animateLabel(elem);
	});
	
	$(".animate-label input, .animate-label textarea").each(function(i,elem){
		animateLabel($(elem));
	});
	
	function animateLabel(elem){
		var parent = elem.parent(".animate-label");
		var clase = "label-top";
		parent.removeClass(clase);
		if(elem.is(":focus") || elem.val()){
			parent.addClass(clase);
		}
	}
	
	
	//Fixed menu al hacer scroll
	if($("#menu").length){
		var theMenu = $('#menu'),
			scrollClass = 'fixed-hdr',
			activateAtY = $("#hdr").outerHeight() - theMenu.outerHeight();

		$(window).on("load scroll resize",function() {
			if($(window).scrollTop() > activateAtY) {
				theMenu.addClass(scrollClass);
			} else {
				theMenu.removeClass(scrollClass);
			}
		});
	}
	
	
	//Funcion de ver mas y menos
	$(".btn-toggle-info").click(function(){
		var theButton = $(this),
			infoContainerID = theButton.data("infocontainer"),
			theContainer = $(infoContainerID);
		
		if(theContainer.is(".active")){
			cerrarInfo(infoContainerID,function(){return;});
			return false;
		}
		
		var tieneGrupo = false,
			grupoName = "";
		
		/*Se bloquean los botones que abren el mismo bloque o el mismo grupo para evitar choque mientras se
		completa la animacion*/
		var selectorBotones = ".btn-toggle-info[data-infocontainer='"+infoContainerID+"']";
		if(theContainer.is("[data-grupo]")){
			tieneGrupo = true;
			grupoName = theContainer.data("grupo");
			selectorBotones = ".btn-toggle-info[data-grupo='"+grupoName+"']";
		}
		$(selectorBotones).attr("disabled", true);
		
		/*Si hay un bloque del mismo grupo que esta abierto se cierra antes de abrir el otro*/
		if(tieneGrupo && $(".more-info-container[data-grupo='"+grupoName+"'].active").length){
			$(".more-info-container[data-grupo='"+grupoName+"'].active").hide();
			$(selectorBotones).removeClass("active");
			$(".more-info-container[data-grupo='"+grupoName+"']").removeClass("active");
			mostrarInfo(infoContainerID, false);
		}else{
			mostrarInfo(infoContainerID, true);
		}
		
	});
	
	function mostrarInfo(infoContainerID, animar){
		var theContainer = $(infoContainerID);
		
		var tieneGrupo = false,
			grupoName = "";
		
		var selectorBotones = ".btn-toggle-info[data-infocontainer='"+infoContainerID+"']";
		if(theContainer.is("[data-grupo]")){
			tieneGrupo = true;
			grupoName = theContainer.data("grupo");
			selectorBotones = ".btn-toggle-info[data-grupo='"+grupoName+"']";
		}
		if(animar){
			$(theContainer).slideDown(300, function(){
				$(".btn-toggle-info[data-infocontainer='"+infoContainerID+"']").addClass("active");
				theContainer.addClass("active");
				$(selectorBotones).removeAttr("disabled");
				initSliders();
				setBorderClass(".titulares",".item","r-border");
				scrollToElement(theContainer);
			});
		}else{
			$(theContainer).show();
			$(".btn-toggle-info[data-infocontainer='"+infoContainerID+"']").addClass("active");
			theContainer.addClass("active");
			$(selectorBotones).removeAttr("disabled");
			initSliders();
			setBorderClass(".titulares",".item","r-border");
			scrollToElement(theContainer);
		}
	}
	
	function cerrarInfo(infoContainerID){
		var theContainer = $(infoContainerID);
		theContainer.slideUp(300,function(){
			theContainer.removeClass("active");
			$(".btn-toggle-info[data-infocontainer='"+infoContainerID+"']").removeClass("active");
		});
	}
	
	$(".btn-close-info").click(function(e){
		e.preventDefault();
		infoContainerID = $(this).data("infocontainer");
		cerrarInfo(infoContainerID);
	});
	
	
	//Inicializar slider al inicio
	initSliders();
	
	$(window).on('activate.bs.scrollspy', function(e) {
		history.replaceState({}, "", $('#menu-navbar .nav-item .active').attr("href"));
	});
	
	
	//Mostrar media en testimonios
	$("[data-mediashow]").click(function(){
		$("#media .item, [data-mediashow]").removeClass("active");
		var mediaID = $(this).data("mediashow");
		var item = $(mediaID);
		item.addClass("active");
		$("[data-mediashow='"+mediaID+"']").addClass("active");
		scrollToElement($("#media"));
		pauseAllVideos();
	});
	
	
	//Crea un array con lista de videos a inicializar
	//a partir de elementos con el data-media-video
	$("#media .item [data-media-video]").each(function(i,element){
		var elem = $(this);
		var url = elem.data("media-video");
		
		playerInfoList[i] = {
			id: elem.attr("id"),
			height: '390',
			width: '640',
			videoId: YouTubeGetID(url)
		}
	});
	
	
	if($(".box-landing .slider").length){
		setSliderHeight();
		$(".box-landing .slider").slick({
			arrows: false,
			autoplay: true,
			dots: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplaySpeed: 4000,
			fade: true
		});
		
		$(window).resize(function(){setSliderHeight();});
		function setSliderHeight(){
			var h = $("#hdr").outerHeight() + $("#footer").outerHeight();
			$(".box-landing .slider .item").css("height","calc(100vh - "+h+"px)");
		}
	}
	
});


function scrollToElement(element){
	var position = element.offset().top - $("#menu").outerHeight();
	$(window).scrollTop(position);
}


function initSliders(){
	//Slider Contactos
	if($(".slider-contacts .slider").length){
		$(".slider-contacts .slider").not('.slick-initialized').slick({
			infinite: false,
			speed: 300,
			slidesToShow: 3,
			slidesToScroll:	1,
			autoplay: true,
			autoplaySpeed: 3000,
			responsive: [
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
						fade: true
					}
				}
			]
		});
	}
	
	if($(".slider-testimonios .slider").length){
		$(".slider-testimonios .slider").not('.slick-initialized').slick({
			infinite: false,
			speed: 300,
			slidesToShow: 3,
			slidesToScroll:	1,
			autoplay: false,
			responsive: [
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
						fade: true
					}
				}
			]
		});
	}
	
	
	//Slider team
	if($(".slider-team .slider").length){
		$(".slider-team .slider").not('.slick-initialized').slick({
			infinite: false,
			speed: 300,
			slidesToShow: 4,
			slidesToScroll:	1,
			autoplay: true,
			autoplaySpeed: 3000,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1
					}
				}
			]
		});
	}
		
}



//Inicializar validador de formulario de bootstrap
(function() {
	window.addEventListener('load', function() {
		// Fetch all the forms we want to apply custom Bootstrap validation styles to
		var forms = document.getElementsByClassName('needs-validation');
		// Loop over them and prevent submission
		var validation = Array.prototype.filter.call(forms, function(form) {
			form.addEventListener('submit', function(event) {
				if (form.checkValidity() === false) {
					event.preventDefault();
					event.stopPropagation();
				}
				form.classList.add('was-validated');
			}, false);
		});
	}, false);
})();


//Asignar borde en listados responsive
function setBorderClass(containerSelector, itemSelector, borderClass){
	if(!$(containerSelector).find(itemSelector+":visible").length) {
		return false;
	}
	
	$(containerSelector+":visible").each(function(n,elem){
		var container = $(elem);
		var contWidth = container.outerWidth();
		var	colsWidth = container.find(itemSelector).first().outerWidth();
		var colsPerRow = parseInt(contWidth / colsWidth);
		//console.log("width:"+colsWidth + " cols:"+colsPerRow);
		
		container.find(itemSelector).removeClass(borderClass);
		if(colsPerRow < 2){	return true;}
		container.find(itemSelector).addClass(borderClass);
		container.find(itemSelector+":nth-child("+colsPerRow+"n),"+itemSelector+":last-child").removeClass(borderClass);
	});
	
}


function changeSVGExtension(){
	$("img.svg-png").each(function(n,elem){
		var src = $(elem).attr('src');
		if (src.indexOf(".svg") > -1) {
			src = src.replace('.svg','.png' );
		}
		$(elem).attr('src', src);
	});
}


/***** INICIALIZAR VIDEOS *****/
	
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var playersVideoMedia = new Array();

function onYouTubeIframeAPIReady() {
	if (typeof playerInfoList === 'undefined') return;

	for (var i = 0; i < playerInfoList.length; i++) {
		var curplayer = createPlayer(playerInfoList[i]);
		playersVideoMedia[i] = curplayer;
	}
}

function createPlayer(playerInfo) {
	console.log("create");
	return new YT.Player(playerInfo.id, {
		height: playerInfo.height,
		width: playerInfo.width,
		videoId: playerInfo.videoId
	});
}

//Obtiene el ID de video de youtube a partir de una url
function YouTubeGetID(url){
  var ID = '';
  url = url.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
  if(url[2] !== undefined) {
    ID = url[2].split(/[^0-9a-z_\-]/i);
    ID = ID[0];
  }
  else {
    ID = url;
  }
    return ID;
}


//Pausa todos los videos de la lista
function pauseAllVideos() {

		//loop players array to stop them all
		$(playersVideoMedia).each(function (i) {
			//console.log(this);
			this.pauseVideo();
		});

}